package com.example.tictactoe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.tictactoe.databinding.FragmentStartGameBinding

class StartGameFragment : Fragment() {

    private var binding: FragmentStartGameBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentStartGameBinding.inflate(inflater,container,false)

        setUpListeners()

        return binding?.root
    }


    private fun setUpListeners()
    {
        binding?.btnStartGame?.setOnClickListener {

            val boardDimension = binding?.etDimension?.text.toString().toInt()
            val action = StartGameFragmentDirections.actionStartGameFragmentToGameBoardFragment(boardDimension)
            findNavController().navigate(action)

        }
    }

}