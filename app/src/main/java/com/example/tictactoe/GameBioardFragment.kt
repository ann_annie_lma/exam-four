package com.example.tictactoe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.tictactoe.adapter.TicTacToeAdapter
import com.example.tictactoe.databinding.FragmentGameBioardBinding

class GameBioardFragment : Fragment() {

    private var binding: FragmentGameBioardBinding? = null
    private val args : GameBioardFragmentArgs by navArgs()
    lateinit var gameBoardComponentsList:MutableList<Button>

    var gameBoardDimension = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentGameBioardBinding.inflate(inflater,container,false)


        setUpRecyclerView()


        return binding?.root
    }


    fun setUpRecyclerView()
    {
        initBoardList()
        binding?.rvGameBoard?.layoutManager = GridLayoutManager(activity,getDimension())
        val adapter = TicTacToeAdapter()
        binding?.rvGameBoard?.adapter = adapter
        adapter.addData(initBoardList())
    }




    private fun getDimension():Int
    {
       gameBoardDimension = args.gameBoardDimension
        return gameBoardDimension

    }

    fun initBoardList():MutableList<Button>
    {
        val dimenstionSquare = getDimension() * getDimension()
        gameBoardComponentsList = mutableListOf()
        for(i in 1..dimenstionSquare)
        {
            gameBoardComponentsList.add(Button(activity))

        }

        gameBoardComponentsList.forEachIndexed { index, imageButton ->
            imageButton.id = index + 1
        }

        return gameBoardComponentsList
    }


}