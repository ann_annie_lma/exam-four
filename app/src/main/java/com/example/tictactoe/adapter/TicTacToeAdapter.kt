package com.example.tictactoe.adapter

import android.media.Image
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.tictactoe.R
import com.example.tictactoe.databinding.GameboardItemBinding
import androidx.appcompat.app.AppCompatActivity




class TicTacToeAdapter:RecyclerView.Adapter<TicTacToeAdapter.TicTacToeViewHolder>() {


    private val gameBoardComponents = mutableListOf<Button>()

    enum class Turn
    {
        NOUGHT,
        CROSS
    }

    companion object
    {
        const val NOUGHT = "O"
        const val CROSS = "X"
    }


    private val firsTurn = Turn.CROSS
    private var currentTurn = Turn.NOUGHT



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicTacToeViewHolder {
        return TicTacToeViewHolder(
            GameboardItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TicTacToeViewHolder, position: Int) {
        holder.bindData()
    }


    override fun getItemCount(): Int = gameBoardComponents.size

    inner class TicTacToeViewHolder(val binding: GameboardItemBinding) :
        RecyclerView.ViewHolder(binding.root){

       lateinit var currentItem:Button

        fun bindData() {

            currentItem = gameBoardComponents[adapterPosition]

            currentItem.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            binding.gameboardComponent.addView(currentItem)

            currentItem.setOnClickListener {
                addBoard(currentItem)

                if(checkforWin(NOUGHT))
                {
                    //Toast.makeText(it.context,"Nought is the Winner!",Toast.LENGTH_SHORT).show()
                }

                if(checkforWin(CROSS))
                {
                    //Toast.makeText(it.context,"Cross is the Winner!",Toast.LENGTH_SHORT).show()
                }

                if(fullBoard())
                {
                    Toast.makeText(it.context,"Draw",Toast.LENGTH_SHORT).show()
                }
            }

        }

        }


    fun addData(cats: MutableList<Button>) {
        gameBoardComponents.clear()
        gameBoardComponents.addAll(cats)
        notifyDataSetChanged()
    }


    private fun checkforWin(nought: String): Boolean {
        return true
    }

    private fun match(button:Button,symbol:String):Boolean = button.text == symbol

    private fun addBoard(button:Button)
    {
        if(button.text != "")
            return

        if(currentTurn == Turn.NOUGHT)
        {
            button.text = NOUGHT
            currentTurn = Turn.CROSS
        }

        else if(currentTurn == Turn.CROSS)
        {
            button.text = CROSS
            currentTurn = Turn.NOUGHT
        }

    }

    private fun fullBoard():Boolean
    {
        for(button in gameBoardComponents)
        {
            if(button.text == "")
                return false
        }

        return true
    }

}